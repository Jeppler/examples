# AES-SIV

Run self-test:

~~~
python3 -m Crypto.SelfTest
~~~

See: https://pycryptodome.readthedocs.io/en/latest/src/installation.html
