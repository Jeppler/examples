from Crypto.Cipher import AES
from Crypto.Random import get_random_bytes
from base64 import b64encode

def convert(value: bytes) -> str:
  return b64encode(value).decode('utf-8')

def encrypt():
  # The additional data is also called `header`
  additional_data = b"This is my additional data"
  plaintext = b"This is my secret text"

  # AES-SIV requires a key which is twice as long as 128 bit = 16 bytes. 32 bytes is twice as long.
  # The sercet is often times also called `key`
  secret = get_random_bytes(16 * 2)

  nonce = get_random_bytes(16)

  cipher = AES.new(secret, AES.MODE_SIV, nonce=nonce)
  cipher.update(additional_data)

  ciphertext, tag = cipher.encrypt_and_digest(plaintext)

  ciphertext_str = convert(ciphertext)
  tag_str = convert(tag)
  nonce_str = convert(nonce)
  additional_data_str = additional_data.decode('utf-8') #convert(additional_data)
  
  print(f"Ciphertext:\t\t {ciphertext_str}")
  print(f"Tag:\t\t\t {tag_str}")
  print(f"Nonce:\t\t\t {nonce_str}")
  print(f"Additional Data:\t {additional_data_str}")

def main():
  encrypt()

if __name__ == "__main__":
  main()
