#!/usr/bin/env bash

function debug() {
	echo "Debug Mode"
	while :
	do
		echo "Press [CTRL+C] to stop.."
		sleep 120
	done
}

function start_server() {
	echo "Server Mode"
	
	# start apacheds in background
	/etc/init.d/apacheds start
	
	# wait for the server to start
	sleep 5
	
	# create users
	ldapadd -v -h localhost:10389 -c -x -D uid=admin,ou=system -w secret -f /ldap/users.ldif
	
	# wait for the user to be created
	sleep 5
	
	# stop the server
  /etc/init.d/apacheds stop
  
	# remove the --background parameter to start apacheds in the forground
	sed -i 's/--background//' /etc/init.d/apacheds
	
	/etc/init.d/apacheds start
}


if [[ "$APP_MODE" == "server" ]]
then
	start_server
else
	debug
fi
