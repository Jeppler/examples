# LDAP Server

Manual start:

~~~
/etc/init.d/apacheds start
~~~

![Apache Directory Studio - Network Connection](ldap_connection_network.png)

![Apache Directory Studio - Authentication](ldap_connection_authentication.png)

Copy and rename variable.env to .env. The value in the .env file defines the modus the docker container will be started in.
